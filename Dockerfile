# use a node base image
FROM node:7-onbuild

# set maintainer
LABEL maintainer "miiro@getintodevops.com"

# set a health check
HEALTHCHECK --interval=5s \
            --timeout=5s \
            CMD curl -f http://54.147.234.242:8080 || exit 1


#run tests
RUN npm test
# tell docker what port to expose
EXPOSE 8080
